import { NestFactory } from '@nestjs/core';
import { json, urlencoded } from 'express';
import { ServerModule } from './server.module';

async function bootstrap() {
    const app = await NestFactory.create(ServerModule);

    app.enableCors({
        origin: true,
        credentials: true,
    });

    app.use(json({ limit: '5000000000mb' }));
    app.use(urlencoded({ extended: true, parameterLimit: 1000000 }));

    await app.listen(process.env.SERVER_PORT);
}
bootstrap();
