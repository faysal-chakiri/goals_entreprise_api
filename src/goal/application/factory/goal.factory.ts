import Goal from '../../domaine/goal';
import GoalCommand from '../command/goal.command';

export default class GoalFactory {
    public createGoal(goalCommand: GoalCommand): Goal {
        return new Goal(
            goalCommand.id,
            goalCommand.name_goal,
            goalCommand.start_date,
            goalCommand.end_date,
            goalCommand.type_goal,
        );
    }
}
