export default class GoalCommand {
    public id: number;
    public name_goal: string;
    public start_date: string;
    public end_date: string;
    public type_goal: string;
}
