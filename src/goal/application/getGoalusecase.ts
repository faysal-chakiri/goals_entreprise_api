import { Inject, Injectable } from '@nestjs/common';
import Goal from '../domaine/goal';
import { IGoalRepository } from '../domaine/goal.repository';

@Injectable()
export default class GetGoalUseCase {
    constructor(@Inject('IGoalRepository') private goalRepository: IGoalRepository) {}

    public handler(id: number): Promise<Goal> {
        const goal = this.goalRepository.get(id);
        return goal;
    }
}
