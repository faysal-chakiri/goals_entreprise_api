import { Injectable, Inject } from '@nestjs/common';
import GoalFactory from './factory/goal.factory';
import { IGoalRepository } from '../domaine/goal.repository';
import GoalCommand from './command/goal.command';

@Injectable()
export default class UpdateGoalUseCase {
    constructor(@Inject('IGoalRepository') private goalRepository: IGoalRepository, private goalFactory: GoalFactory) {}

    public handler(id: number, goalCommand: GoalCommand): Promise<void> {
        const goalUpdate = this.goalFactory.createGoal(goalCommand);
        return this.goalRepository.update(id, goalUpdate);
    }
}
