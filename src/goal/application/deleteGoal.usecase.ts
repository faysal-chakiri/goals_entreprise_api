import { Inject, Injectable } from '@nestjs/common';
import { DeleteResult } from 'typeorm';
import { IGoalRepository } from '../domaine/goal.repository';

@Injectable()
export default class DeleteGoalUseCase {
    constructor(@Inject('IGoalRepository') private goalRepository: IGoalRepository) {}

    public handler(id: number): Promise<DeleteResult> {
        return this.goalRepository.delete(id);
    }
}
