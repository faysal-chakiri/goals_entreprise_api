import { Inject, Injectable } from '@nestjs/common';
import { IGoalRepository } from '../domaine/goal.repository';
import Goal from '../domaine/goal';
import GoalCommand from './command/goal.command';
import GoalFactory from './factory/goal.factory';

@Injectable()
export default class CreateGoalUseCase {
    constructor(@Inject('IGoalRepository') private goalRepository: IGoalRepository, private goalFactory: GoalFactory) {}

    public handler(goalCommand: GoalCommand): Promise<Goal> {
        const goal = this.goalFactory.createGoal(goalCommand);
        return this.goalRepository.create(goal);
    }
}
