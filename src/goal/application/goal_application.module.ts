import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import DomainModule from '../domaine/domaine.module';
import { GoalEntity } from '../domaine/goal.entity';
import { GoalService } from '../infrastructure/goal.service';
import CreateGoalUseCase from './createGoal.usecase';
import DeleteTopUseCase from './deleteGoal.usecase';
import GoalFactory from './factory/goal.factory';
import GetAllTopsUseCase from './getAllGoals.usecase';
import GetGoalUseCase from './getGoalusecase';
import UpdateGoalUseCase from './updateGoal.usecase';

@Module({
    imports: [DomainModule, TypeOrmModule.forFeature([GoalEntity])],
    providers: [
        GoalFactory,
        { provide: 'IGoalRepository', useClass: GoalService },
        CreateGoalUseCase,
        GetGoalUseCase,
        GetAllTopsUseCase,
        DeleteTopUseCase,
        UpdateGoalUseCase,
    ],
    exports: [CreateGoalUseCase, GetGoalUseCase, GetAllTopsUseCase, DeleteTopUseCase, UpdateGoalUseCase],
})
export default class ApplicationModule {}
