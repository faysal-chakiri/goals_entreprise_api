import { Inject, Injectable } from '@nestjs/common';
import { IGoalRepository } from '../domaine/goal.repository';

@Injectable()
export default class GetAllTopsUseCase {
    constructor(@Inject('IGoalRepository') private goalRepository: IGoalRepository) {}

    public handler() {
        const goals = this.goalRepository.getAll();
        return goals;
    }
}
