import { Module } from '@nestjs/common';

import { TypeOrmModule } from '@nestjs/typeorm';
import ApplicationModule from '../application/goal_application.module';
import { GoalController } from './goal.controller';
import { GoalService } from './goal.service';
import { GoalEntity } from '../domaine/goal.entity';

@Module({
    controllers: [GoalController],
    providers: [GoalService],
    imports: [TypeOrmModule.forFeature([GoalEntity]), ApplicationModule],
})
export class GoalModule {}
