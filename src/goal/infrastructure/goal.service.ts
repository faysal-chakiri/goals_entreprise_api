import { Body, HttpStatus, Injectable, Param, Patch, Res } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import Goal from '../domaine/goal';
import { GoalEntity } from '../domaine/goal.entity';
import GoalMapper from '../domaine/goal.mapper';
import { IGoalRepository } from '../domaine/goal.repository';
import GoalCommand from '../application/command/goal.command';
import { Response } from 'express';

@Injectable()
export class GoalService implements IGoalRepository {
    constructor(
        @InjectRepository(GoalEntity)
        private goalRepository: Repository<GoalEntity>,
    ) {}

    public async create(goal: Goal): Promise<Goal> {
        const goalCreated = this.goalRepository.create({
            id: goal.getId(),
            name_goal: goal.getName_goal(),
            start_date: goal.getStart_date(),
            end_date: goal.getEnd_date(),
            type_goal: goal.getType_goal(),
        });

        await this.goalRepository.insert(goalCreated);
        return GoalMapper.toDomain(goalCreated);
    }
    public async getAll(): Promise<Goal[]> {
        const goalsGetted: GoalEntity[] = await this.goalRepository.find();
        return GoalMapper.toDomains(goalsGetted);
    }

    public async get(id: number): Promise<Goal> {
        const goalGetted: GoalEntity = await this.goalRepository.findOne({
            where: { id },
        });
        return GoalMapper.toDomain(goalGetted);
    }

    public async delete(id: number): Promise<DeleteResult> {
        return await this.goalRepository.delete({ id });
    }

    public async update(id: number, goal: Goal): Promise<void> {
        await this.goalRepository.update(
            { id },
            {
                id: goal.getId(),
                name_goal: goal.getName_goal(),
                start_date: goal.getStart_date(),
                end_date: goal.getEnd_date(),
                type_goal: goal.getType_goal(),
            },
        );
    }
}
