import { Body, Controller, Delete, Get, HttpStatus, Param, Patch, Post, Req, Res } from '@nestjs/common';
import { Request, Response } from 'express';
import CreateGoalUseCase from '../application/createGoal.usecase';
// import DeleteGoalUseCase from '../application/deleteGoal.usecase';
import GetAllGoalsUseCase from '../application/getAllGoals.usecase';
import GetGoalUseCase from '../application/getGoalusecase';
import GoalCommand from '../application/command/goal.command';
import DeleteGoalUseCase from '../application/deleteGoal.usecase';
import UpdateGoalUseCase from '../application/updateGoal.usecase';

@Controller('goal')
export class GoalController {
    constructor(
        private createGoalUseCase: CreateGoalUseCase,
        private getGoalUseCase: GetGoalUseCase,
        private deleteGoalUseCase: DeleteGoalUseCase,
        private getGoalsUseCase: GetAllGoalsUseCase,
        private updateGoalUseCase: UpdateGoalUseCase,
    ) {}

    @Post()
    public async createGoal(@Res() response: Response, @Body() goal: GoalCommand) {
        try {
            const goalFound = await this.getGoalUseCase.handler(goal.id);
            if (goalFound) {
                return response.status(HttpStatus.BAD_REQUEST).json({
                    messageFr: `Le goal: ${goal.name_goal} existe déjà`,
                    code: HttpStatus.BAD_REQUEST,
                });
            }
            const goalCreated = this.createGoalUseCase.handler(goal);
            return response.status(HttpStatus.CREATED).json(goalCreated);
        } catch (err) {
            return response.status(HttpStatus.BAD_REQUEST).json(err);
        }
    }

    @Get('/getAll')
    public async getGoals(@Req() request: Request, @Res() response: Response) {
        try {
            const goalsFound = await this.getGoalsUseCase.handler();

            return response.status(HttpStatus.OK).json(goalsFound);
        } catch (err) {
            return response.status(HttpStatus.BAD_REQUEST).json(err);
        }
    }

    @Get('/getOne/:id')
    public async getGoal(@Req() request: Request, @Res() response: Response, @Param() id: number) {
        try {
            const goalFound = await this.getGoalUseCase.handler(id);
            if (goalFound == undefined) {
                return response.status(HttpStatus.BAD_REQUEST).json({
                    messageFr: `Le goal avec id: ${goalFound.id} n'existe pas`,
                    code: HttpStatus.BAD_REQUEST,
                });
            }
            return response.status(HttpStatus.OK).json(goalFound);
        } catch (err) {
            return response.status(HttpStatus.BAD_REQUEST).json(err);
        }
    }

    @Delete('/:id')
    public async deleteGoal(@Res() response: Response, @Param('id') id: number) {
        const goalFound = await this.getGoalUseCase.handler(id);
        try {
            if (!goalFound) {
                return response.status(HttpStatus.NOT_FOUND).json({
                    message: `goal avec l'id ${id} n'existe pass`,
                    code: 400,
                });
            }
            await this.deleteGoalUseCase.handler(id);
            return response.status(HttpStatus.OK).json({
                message: `goal avec l'id ${id} est supprimé`,
            });
        } catch (err) {
            return response.status(HttpStatus.BAD_REQUEST).json(err);
        }
    }

    @Patch('/:id')
    public async updateGoal(@Res() response: Response, @Param('id') id: number, @Body() goalCommand: GoalCommand) {
        try {
            const goalFound = await this.getGoalUseCase.handler(id);
            if (!goalFound) {
                return response.status(HttpStatus.BAD_REQUEST).json({
                    messageFr: `Le goal avec id: ${goalFound.id} n'existe pas`,
                    code: HttpStatus.BAD_REQUEST,
                });
            }
            return response.status(HttpStatus.OK).json(goalFound);
        } catch (err) {
            const goalUpdate = await this.updateGoalUseCase.handler(id, goalCommand);

            return response.status(HttpStatus.OK).json(goalUpdate);
        }

        // try {
        //     const goalFound = await this.getGoalUseCase.handler(goal, id_site);
        //     const goalFoundBody = await this.getGoalUseCase.handler(goalCommand.goal, parseInt(goalCommand.id_site));

        //     if (!goalFound) {
        //         return response.status(HttpStatus.BAD_REQUEST).json({
        //             messageFr: `Le goal: ${goal} et l'id site: ${id_site} n'existent pas`,
        //             messageAn: `The goal: ${goal} and site id: ${id_site} not found`,
        //             code: HttpStatus.BAD_REQUEST,
        //         });
        //     } else if (goalFoundBody) {
        //         if (goalCommand.goal == goal && parseInt(goalCommand.id_site) == id_site) {
        //             const goalUpdate = await this.updateGoalUseCase.handler(goal, id_site, goalCommand);
        //             return response.status(HttpStatus.OK).json(goalUpdate);
        //         }
        //         return response.status(HttpStatus.BAD_REQUEST).json({
        //             messageFr: `Le goal: ${goalCommand.goal} et l'id site: ${goalCommand.id_site} existent déjà`,
        //             messageAn: `The goal: ${goalCommand.goal} and site id: ${goalCommand.id_site} already exists`,
        //             code: HttpStatus.BAD_REQUEST,
        //         });
        //     }

        // } catch (err) {
        //     return response.status(HttpStatus.BAD_REQUEST).json(err);
        // }
    }
}
