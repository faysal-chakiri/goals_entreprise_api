import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('name_goal')
export class GoalEntity {
    @PrimaryGeneratedColumn()
    id: number;
    @Column()
    name_goal: string;
    @Column()
    start_date: string;
    @Column()
    end_date: string;
    @Column()
    type_goal: string;
}
