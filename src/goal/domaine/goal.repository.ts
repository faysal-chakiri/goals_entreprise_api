import { DeleteResult } from 'typeorm';
import Goal from './goal';

export interface IGoalRepository {
    create(goal: Goal): Promise<Goal>;
    get(id: number): Promise<Goal>;
    getAll(): Promise<Goal[]>;
    delete(id: number): Promise<DeleteResult>;
    update(id: number, goal: Goal): Promise<void>;
}
