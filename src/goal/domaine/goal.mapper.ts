import Goal from './goal';
import { GoalEntity } from './goal.entity';

export default class GoalMapper {
    public static toDomain(goalEntity: GoalEntity): Goal {
        if (!goalEntity) {
            return;
        }
        const goal = new Goal(
            goalEntity.id,
            goalEntity.name_goal,
            goalEntity.start_date,
            goalEntity.end_date,
            goalEntity.type_goal,
        );
        return goal;
    }

    public static toDomains(goalEntity: GoalEntity[]): Goal[] {
        const goals = new Array<Goal>();
        goalEntity.forEach((goalEntity) => {
            const goal = this.toDomain(goalEntity);
            goals.push(goal);
        });
        return goals;
    }
}
