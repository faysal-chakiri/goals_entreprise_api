export default class Goal {
    public readonly id: number;
    private readonly name_goal: string;
    private readonly start_date: string;
    private readonly end_date: string;
    private readonly type_goal: string;

    constructor(id: number, name_goal: string, start_date: string, end_date: string, type_goal: string) {
        this.id = id;
        this.name_goal = name_goal;
        this.start_date = start_date;
        this.end_date = end_date;
        this.type_goal = type_goal;
    }

    public getId(): number {
        return this.id;
    }

    public getName_goal(): string {
        return this.name_goal;
    }

    public getStart_date(): string {
        return this.start_date;
    }

    public getEnd_date(): string {
        return this.end_date;
    }

    public getType_goal(): string {
        return this.type_goal;
    }
}
