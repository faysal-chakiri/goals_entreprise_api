import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GoalEntity } from './goal/domaine/goal.entity';

@Module({
    imports: [
        ConfigModule.forRoot(),
        TypeOrmModule.forRoot({
            type: 'mysql',
            host: process.env.DB_HOST,
            port: parseInt(process.env.DB_PORT, 10) || 3306,
            username: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_NAME,
            entities: [`${__dirname}/**/*.entity{.ts,.js}`],
            timezone: 'utc',
            synchronize: false,
        }),
        TypeOrmModule.forFeature([GoalEntity]),
    ],
})
export class DatabaseModule {}
