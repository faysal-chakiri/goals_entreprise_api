import { Module } from '@nestjs/common';
import { GoalModule } from './goal/infrastructure/goal.module';
import { DatabaseModule } from './database.module';

@Module({
    imports: [GoalModule, DatabaseModule],
})
export class ServerModule {}
